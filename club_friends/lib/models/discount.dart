class Discount {
  int quantity;
  int min;
  int max;

  Discount({quantity, min, max})
      : this.quantity = (quantity != null) ? quantity : 25,
        this.min = (min != null) ? min : 20,
        this.max = (max != null) ? max : 60;
}
