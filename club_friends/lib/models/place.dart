import 'dart:math';

import 'discount.dart';

class Place {
  String name;
  String image;
  Function onTap;
  Discount discounts;
  int priceRange;
  int likes;
  double stars;
  int reviewers;
  double howFar;
  bool infoDark;
  String about;
  String openHours;

  static Random _r = Random();

  static _defaultValues(key) => {
        'name': 'A Place',
        'image': '',
        'onTap': () {},
        'discount': Discount(
            quantity: _r.nextInt(6) * 10, min: _r.nextInt(5) * 10, max: 50),
        'priceRange': 2,
        'likes': _r.nextInt(5000),
        'stars': _r.nextDouble() * 2 + 3,
        'reviewers': _r.nextInt(3000),
        'howFar': _r.nextInt(150) / 10,
        'infoDark': false,
        'about': 'Náš klub se pyšní tím nejlepším prostředím ve městě!' +
            'Luxusní kokteily, skvělé eventy, DJ mezinárodní třídy bar plný ' +
            'dobrého pití a skvělá atmosféra!',
        'openHours':
            'po - čt // 16:00 - 24:00\npá - so // 15:00 - 03:00\nne // zavřeno'
      }[key];

  Place({
    String name,
    String image,
    Function onTap,
    Discount discounts,
    int priceRange,
    int likes,
    double stars,
    int reviewers,
    double howFar,
    bool infoDark,
    String about,
    String openHours,
  })  : name = name ?? _defaultValues('name'),
        image = image ?? _defaultValues('image'),
        onTap = onTap ?? _defaultValues('onTap'),
        discounts = discounts ?? _defaultValues('discount'),
        priceRange = priceRange ?? _defaultValues('priceRange'),
        likes = likes ?? _defaultValues('likes'),
        stars = stars ?? _defaultValues('stars'),
        reviewers = reviewers ?? _defaultValues('reviews'),
        howFar = howFar ?? _defaultValues('howFar'),
        infoDark = infoDark ?? _defaultValues('infoDark'),
        about = about ?? _defaultValues('about'),
        openHours = openHours ?? _defaultValues('openHours');
}
