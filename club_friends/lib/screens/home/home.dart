import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:club_friends/screens/home/tabs/clubs.dart';
import 'package:club_friends/screens/home/tabs/events.dart';
import 'package:club_friends/screens/home/tabs/pubs.dart';
import 'package:club_friends/screens/home/tabs/tea_rooms.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: choices.length,
      child: Scaffold(
        appBar: AppBar(
          title: const Text(
            'Club Friends',
            style: TextStyle(color: Colors.deepPurple),
          ),
          backgroundColor: Colors.yellow,
          bottom: TabBar(
            isScrollable: true,
            indicatorColor: Colors.deepPurple,
            tabs: tabNames.map((String name) {
              return Tab(
                  child:
                      Text(name, style: TextStyle(color: Colors.deepPurple)));
            }).toList(),
          ),
        ),
        body: TabBarView(
          children: [
            Clubs(),
            Pubs(),
            TeaRooms(),
            Events(),
          ],
        ),
      ),
    );
  }
}

const tabNames = [
  'Clubs',
  'Pubs',
  'Tea Rooms',
  'Events',
];

class Choice {
  const Choice({this.title, this.icon});

  final String title;
  final IconData icon;
}

const List<Choice> choices = const <Choice>[
  const Choice(title: 'CAR', icon: Icons.directions_car),
  const Choice(title: 'BICYCLE', icon: Icons.directions_bike),
  const Choice(title: 'TRAIN', icon: Icons.directions_railway),
  const Choice(title: 'WALK', icon: Icons.directions_walk),
];

class ChoiceCard extends StatelessWidget {
  const ChoiceCard({Key key, this.choice}) : super(key: key);

  final Choice choice;

  @override
  Widget build(BuildContext context) {
    final TextStyle textStyle = Theme.of(context).textTheme.display1;
    return Card(
      color: Colors.white,
      child: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Icon(choice.icon, size: 128.0, color: textStyle.color),
            Text(choice.title, style: textStyle),
          ],
        ),
      ),
    );
  }
}
