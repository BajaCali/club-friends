import 'package:club_friends/screens/club/club_screen.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'package:club_friends/models/discount.dart';
import 'package:club_friends/models/place.dart';

class Clubs extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        ClubCard(
          place: Place(
            name: 'Watt',
            image: 'assets/watt.png',
            howFar: 5.6,
            discounts: Discount(),
          ),
        ),
        ClubCard(
          place: Place(
            name: 'Sono',
            image: 'assets/sono.jpg',
            discounts: Discount(quantity: 10, min: 40, max: 60),
            howFar: 4.4,
          ),
        ),
        ClubCard(
          place: Place(
            name: 'Caribic',
            image: 'assets/caribic.jpg',
            howFar: 6.7,
            infoDark: true,
          ),
        )
      ],
    );
  }
}

class ClubCard extends StatelessWidget {
  final Place _place;

  ClubCard({Place place}) : _place = place;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(10),
      child: Card(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        clipBehavior: Clip.antiAlias,
        child: GestureDetector(
          onTap: () {
            Navigator.of(context).push(MaterialPageRoute(
                builder: (BuildContext context) => Club(
                      place: _place,
                    )));
          },
          child: AspectRatio(
            aspectRatio: 3 / 2,
            child: Stack(
              fit: StackFit.expand,
              children: [
                Image.asset(
                  _place.image,
                  fit: BoxFit.fill,
                ),
                Positioned(
                  left: 16,
                  top: 8,
                  child: Text(
                    _place.name,
                    style: GoogleFonts.montserrat(
                        textStyle:
                            TextStyle(fontSize: 32, color: Colors.white)),
                  ),
                ),
                Positioned(
                  bottom: 16,
                  right: 16,
                  child: Container(
                    width: 100,
                    child: Table(
                      // border: TableBorder.all(),
                      defaultVerticalAlignment:
                          TableCellVerticalAlignment.middle,
                      columnWidths: {0: FractionColumnWidth(.3)},
                      children: [
                        TableRow(
                          children: [
                            Icon(
                              Icons.location_on,
                              color: (_place.infoDark) ? null : Colors.white,
                            ),
                            Text(
                              _place.howFar.toString() + ' km',
                              style: GoogleFonts.montserrat(
                                  textStyle: TextStyle(
                                      fontSize: 16,
                                      color: (_place.infoDark)
                                          ? null
                                          : Colors.white)),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            Icon(
                              Icons.confirmation_number,
                              color: (_place.infoDark) ? null : Colors.white,
                            ),
                            Text(
                              _place.discounts.quantity.toString() + '+ slev',
                              style: GoogleFonts.montserrat(
                                  textStyle: TextStyle(
                                      fontSize: 16,
                                      color: (_place.infoDark)
                                          ? null
                                          : Colors.white)),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                Positioned(
                  left: 16,
                  bottom: 16,
                  child: RaisedButton(
                    elevation: 0,
                    // padding: EdgeInsets.all(value),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(100)),
                    color: Colors.deepPurple,
                    child: Text('Slevy ' +
                        _place.discounts.min.toString() +
                        ' - ' +
                        _place.discounts.max.toString() +
                        ' %'),
                    onPressed: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (BuildContext context) => Club(
                                place: _place,
                              )));
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
