import 'package:club_friends/models/place.dart';
import 'package:flutter/material.dart';

import 'package:club_friends/screens/discount/discount_screen.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';

class Club extends StatelessWidget {
  final Place _place;

  Club({Place place}) : _place = place;

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
    return Scaffold(
      body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            SliverAppBar(
              expandedHeight: 200.0,
              floating: false,
              pinned: true,
              flexibleSpace: FlexibleSpaceBar(
                centerTitle: true,
                title: Text(
                  _place.name,
                  style: GoogleFonts.montserrat(
                    textStyle: TextStyle(fontSize: 32, color: Colors.white),
                  ),
                ),
                background: Image.asset(
                  _place.image,
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ];
        },
        body: ListView(
          padding: EdgeInsets.fromLTRB(16, 20, 16, 0),
          children: [
            Padding(
              padding: const EdgeInsets.only(bottom: 20),
              child: Row(
                children: <Widget>[
                  Icon(
                    Icons.location_on,
                  ),
                  Text(
                    _place.howFar.toString() + ' km',
                    style: GoogleFonts.montserrat(
                      textStyle: TextStyle(
                        fontSize: 16,
                      ),
                    ),
                  ),
                  Expanded(
                    child: Container(),
                  ),
                  Heart(),
                ],
              ),
            ),
            // Popis podniku
            Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(bottom: 16.0),
                    child: Row(
                      children: <Widget>[
                        Icon(Icons.format_quote),
                        Padding(
                          padding: const EdgeInsets.only(left: 8.0),
                          child: Text(
                            'Popis podniku',
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 16.0),
                    child: Text(_place.about),
                  ),
                ],
              ),
            ),
            // open hours
            Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(bottom: 16.0),
                    child: Row(
                      children: <Widget>[
                        Icon(Icons.schedule),
                        Padding(
                          padding: const EdgeInsets.only(left: 8.0),
                          child: Text(
                            'Otevírací hodiny',
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 16.0),
                    child: Text(_place.openHours),
                  ),
                ],
              ),
            ),
            RaisedButton(
              splashColor: Colors.yellowAccent,
              elevation: 3,
              color: Colors.deepPurple,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(100)),
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => Discount()));
              },
              child: Row(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: Container(
                      height: 40,
                      child: Image.asset(
                        'assets/cocktail.png',
                        fit: BoxFit.contain,
                      ),
                    ),
                  ),
                  Text(
                    '50 % z ceny prvního drinku!',
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                  Expanded(
                    child: Container(),
                  ),
                  Icon(
                    Icons.arrow_forward_ios,
                    color: Colors.white,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class Heart extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _HeartState();
}

class _HeartState extends State<Heart> {
  bool _isPressed = false;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      splashColor: Colors.transparent,
      onTap: () => setState(() {
        _isPressed = !_isPressed;
      }),
      child: (_isPressed) ? Icon(Icons.favorite) : Icon(Icons.favorite_border),
    );
  }
}
