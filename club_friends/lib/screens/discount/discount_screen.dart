import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class Discount extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);
    return WillPopScope(
      onWillPop: () {
        SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
        Navigator.pop(context);
      },
      child: Scaffold(
        backgroundColor: Colors.deepPurple,
        body: Center(
          child: GestureDetector(
            onDoubleTap: () {
              SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
              Navigator.pop(context);
            },
            child: Column(
              children: <Widget>[
                Expanded(child: Container()),
                Card(
                  color: Colors.yellow,
                  child: Container(
                    height: MediaQuery.of(context).size.height * 0.9,
                    width: MediaQuery.of(context).size.width * 0.8,
                    child: Center(
                        child: Container(
                      height: 200,
                      width: 200,
                      child: Image.asset(
                        'assets/cocktail.png',
                        fit: BoxFit.contain,
                      ),
                    )),
                  ),
                ),
                Expanded(child: Container()),
                Text(
                  'Double tap to cancel.',
                  style: TextStyle(color: Colors.black45),
                ),
                Expanded(child: Container()),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
